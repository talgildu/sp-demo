# Windows IIS Sample Shibboleth Service Provider 3.0

Version (Draft): 0.9 - 09.03.2020

---

## Introduction

This guide describes the process of making an existing ASP.NET MVC 5 web application hosted in Internet
Information Services (IIS) on a Windows machine to a Service Provider (SP) which uses Talgildur Samleiki _Test_
Identity Provider (IDP) - https://innrita.test1.samleiki.fo - to login users. This guide is based upon the Shibboleth
SP, which is also the SP implementation that Talgildur Samleiki (TS) recommends.

Furthermore, this guide is based upon the Shibboleth documentation for installing and configuring Service Providers
- https://wiki.shibboleth.net/confluence/display/SP3/Install+on+Windows.  

_PowerShell_ is used throughout this guide. PowerShell must be launched with _Administrator_ rights - this is achieved
by right-clicking the PowerShell icon, and selecting _Run as administrator_. Note that the same PowerShell instance is
used throughout this guide - the PowerShell _window_ should therefore not be closed.

The structure of this is as follows:

* [Get the Assets](#markdown-header-get-the-assets)
* [Requirements](#markdown-header-requirements)
* [Microsoft ASP.NET MVC 5 Homepage](#markdown-header-microsoft-aspnet-mvc-5-homepage)
* [Hosting the homepage in IIS (*optional*)](#markdown-header-hosting-the-homepage-in-iis-optional)
* [Installing Shibboleth](#markdown-header-installing-shibboleth)
* [Configuring Shibboleth Service Provider](#markdown-header-configuring-shibboleth-service-provider)
* [Login](#markdown-header-login)
* [Remarks](#markdown-header-remarks)
* [Troubleshooting](#markdown-header-troubleshooting)

## Get the Assets
The assets used in this guide is downloaded using PowerShell (run as administrator):

```powershell 
> mkdir /shib-service-provider
> cd /shib-service-provider

> git clone https://bitbucket.org/klintra-ft/sp-demo.git

> cd sp-demo\demo-sp-deployment-windows-iis-3_0
> dir
```

The assets is structured as follows:

* **Source/** - a *Visual Studio 2017* (VS2017) solution containing a simple and standard _out-of-the-box_ ASP.NET MVC 5 
web site
* **Release/** - release bits (*publish*) of the VS2017 solution
* **Shibboleth/** - contains pre-configured XML documents og keys, that are used in the configuration of the Shibboleth SP

## Requirements
This guide is based upon a **Windows Server 2012 R2** and **IIS 7**. The minimum requirements for installing the
Shibboleth SP are:

* Windows Server 2008 R2 or later
* IIS 6 or later
* IIS Management Scripts and Tools on the Windows machine
* ISAPI Extensions og ISAPI Filters on the Windows machine

A more detailed description of the minimum requirement for running the Shibboleth SP can be found here -
https://wiki.shibboleth.net/confluence/display/SP3/SystemRequirements.

## Microsoft ASP.NET MVC 5 Homepage
The web application used in this guide is a simple ASP.NET MVC 5 website. The website is based on the project template
that ships with VS2017 out of the box (by doing _File > New Project_). This website is used as an SP for the simplicity
of this guide. It is also possible to use an existing web site hosted in IIS as the SP instead - however, this does
require some adjustments to the files that are shipped in this guide.

This is how the front page of the web site looks like:

***
![Skíggjamynd av ASP.NET MVC 5 heimasíðuni][asp-net-mvc-sp-screenshot]
***

The __Secure__ page is _protected_, and in order to access this page the user has to be logged in using an IDP.

Some minor adjustments have been made to the website:

* A **Show Server Variables...** button is inserted. Clicking this button results in all *Server Variables* being displayed on
the page. Among the variables it is possible to see the variables that are provided by Shibboleth
* A **Logout from IDP** button is also inserted which enables the user to logout from the IDP
* It is important that Shibboleth SP handles all _requests_ that contain ```.sso```, therefore the following -
```routes.IgnoreRoute("{resource}.sso/{*pathInfo}");``` - is inserted into the ```RouteConfig.cs``` file in the project

## Hosting the homepage in IIS (*optional*)
In this guide the **WebAdministration** module is used in PowerShell. This makes it easier to manage IIS - see link
for more details - https://docs.microsoft.com/en-us/iis/manage/powershell/writing-powershell-commandlets-for-iis. 

It is also possible to use the IIS Manager for all tasks of setting up the web site in IIS.

In order to host the ASP.NET MVC 5 web site in IIS, the following needs to be done:

1. Copy the the web site over to the IIS directory (```C:\inetpub\wwwroot```)
2. Create a new *Site* in IIS
3. Configure the web site to run **HTTPS**

#### 1. Copy the the web site over to the IIS directory (```C:\inetpub\wwwroot```)

Copy the web site in the **Release/** directory to the ```C:\inetpub\wwwroot``` directory:

```powershell
> cd /shib-service-provider/sp-demo/demo-sp-deployment-windows-iis-3_0/
> Copy-Item Release\WebApplication1 -Destination \inetpub\wwwroot\My-SP-Site -Recurse
```

#### 2. Create a new *Site* in IIS

Create a new *Site* in IIS that points to the ```C:\inetpub\wwwroot\My-SP-Site``` directory:

```powershell
# Import features to manage IIS in PowerShell
> Set-ExecutionPolicy unrestricted –force
> Import-Module WebAdministration
> Set-Location IIS:\

# Create the new IIS site
> New-Item IIS:\AppPools\My-SP-Site
> New-Item iis:\Sites\My-SP-Site -bindings @{protocol="http";bindingInformation=":80:demo.test1.samleiki.fo"} -physicalPath C:\inetpub\wwwroot\My-SP-Site
> Set-ItemProperty IIS:\Sites\My-SP-Site -name applicationPool -value My-SP-Site
```

If need be, it is possible to adjust the name and other settings in the PowerShell commands above.

#### 3. Configure the web site to run **HTTPS**

It is required for the IIS site to run on HTTPS. For this purpose a SSL certificate must be created and installed
on the Windows machine. This certificate is then used for HTTPS by the IIS site.

This guide uses a _self-signed_ certificate. In production environments it is important to use dedicated certificates
created for the purpose.

The self-signed certificate is created and installed on the Windows machine using PowerShell:

```powershell
> makecert -r -pe -n "CN=demo.test1.samleiki.fo" -b 01/01/2018 -e 01/01/2028 -eku 1.3.6.1.5.5.7.3.1 -ss my -sr localMachine -sky exchange -sp "Microsoft RSA SChannel Cryptographic Provider" -sy 12
```

A *thumbprint* identifies the certificate which is then used to configure the site to run HTTPS. The thumbprint can be
seen by executing the following command in PowerShell:

```powershell
> dir cert:\localmachine\my

   PSParentPath: Microsoft.PowerShell.Security\Certificate::localmachine\my

Thumbprint                                Subject
----------                                -------
6DEA26E80C0DDC8047904852ECE4B925E210BE84  CN=demo.test1.samleiki.fo
```

(*Note, that the thumbprint is unique, and is therefore not identical to __your__ thumbprint.*)

Once the certificate is installed on the machine, it is ready to be used in the HTTPS setup. First, a new *Web Binding*
needs to be made. Note, that the port used by the new binding needs to be available. In this example port **443** is
used.

```powershell
> New-WebBinding -Name "My-SP-Site" -IP "*" -Port 443 -HostHeader demo.test1.samleiki.fo -Protocol https
```

The next step is to apply the certificate to the newly created *WebBinding*:

```powershell
> cd /SslBindings
> get-item cert:\LocalMachine\MY\6DEA26E80C0DDC8047904852ECE4B925E210BE84 | new-item 0.0.0.0!443
```

In order to make the link - https://demo.test1.samleiki.fo - available in the browser, it is necessary to add a new
_mapping_ in the ```hosts``` file:

```powershell
> notepad.exe C:\Windows\System32\drivers\etc\hosts
```

Add the following line to the ```hosts``` file:

```
127.0.0.1               demo.test1.samleiki.fo
```

Save the file. The web site is now available in the browser - https://demo.test1.samleiki.fo.

## Installing Shibboleth
The Shibboleth SP is available on the Shibboleth web site - https://shibboleth.net/downloads/service-provider/latest/.
It is important that the release version matches the Windows machine version. In this guide the **x64 bit** version
is used. 

The following PowerShell command downloads the 64 bit version of Shibboleth SP:

```powershell  
> cd /shib-service-provider/sp-demo/demo-sp-deployment-windows-iis-3_0/

> wget https://shibboleth.net/downloads/service-provider/3.0.4/win64/shibboleth-sp-3.0.4.2-win64.msi -OutFile shibboleth-sp-3.0.4.2-win64.msi
```

The Shibboleth SP is installed by executing the .MSI file. Note, that all default settings in the installer are applied.

```powershell 
> .\shibboleth-sp-3.0.4.2-win64.msi
```

Remember to include the Shibboleth IIS module by checking the corresponding box in the installer.

The .MSI installer puts all the Shibboleth SP files in the ```C:\opt\``` directory. A certificate is also created which
is used in the Shibboleth SP configuration.

The next step is to enable the Shibboleth SP features in IIS. A detailed description of this approach is available on
the Shibboleth web site - https://wiki.shibboleth.net/confluence/display/SP3/IIS.

The link - https://demo.test1.samleiki.fo/shibboleth.sso/Status - indicates if the Shibboleth SP configuration is 
valid, and working. If everything is setup correctly, the following XML is displayed on the status page:

```xml
<StatusHandler time="2018-03-08T15:06:30Z">
    <Version Xerces-C="3.1.4" XML-Tooling-C="1.6.4" XML-Security-C="1.7.3" OpenSAML-C="2.6.1" Shibboleth="2.6.1"/>
    <Windows version="6.2" build="9200" producttype="Workstation" cpucount="8" memory="32673M"/>
    <SessionCache>
        <OK/>
    </SessionCache>

    ...(some more XML content)

</StatusHandler>
```

In case the status page is not displayed, try some of the following:

* Verify the configuration is generally valid by running %SHIBSP_PREFIX%/sbin/shibd.exe -check from the command line.
* Restart the IIS (iisreset)
* Reboot the machine
* Look in the Shibboleth logs - **C:\opt\shibboleth-sp\var\log\shibboleth\\**

The web site is now ready to be setup as a SP in the federation. 

## Configuring Shibboleth Service Provider
The Shibboleth SP is configured in the ```C:\opt\shibboleth-sp\etc\shibboleth\shibboleth2.xml``` file. An already
configured Shibboleth SP file is included in the assets associated with this guide. Copy this file in to the 
Shibboleth SP configuration directory:

```powershell
>  Copy-Item /shib-service-provider/sp-demo/demo-sp-deployment-windows-iis-3_0/Shibboleth/shibboleth2.xml C:/opt/shibboleth-sp/etc/shibboleth/shibboleth2.xml -Force
```

It is important that the ```id``` value on line 14 (```<Site id="2" name="demo.test1.samleiki.fo" scheme="https" port="443"/>```) 
in ```shibboleth2.xml``` matches the ID of the web site in IIS. This ID can be found in the IIS Manager by selecting
the *My-SP-Site* site on the left and clicking **Advanced settings...** (to the far right).

It is also worth noticing line 39 - ```<Path name="Home/Secure" authType="shibboleth" requireSession="true"/>```. 
This line indicates that the ```Home/Secure``` _resource_ is protected and requires the user to be logged in at a IDP
in order to access the resource.

A detailed description of the Shibboleth SP configuration is located on the web Shibboleth site - 
https://wiki.shibboleth.net/confluence/display/SP3/IIS.

The ```C:\opt\shibboleth-sp\etc\shibboleth\attribute-map.xml``` also needs to be copied:

```powershell
>  Copy-Item /shib-service-provider/sp-demo/demo-sp-deployment-windows-iis-3_0/Shibboleth/attribute-map.xml C:/opt/shibboleth-sp/etc/shibboleth/attribute-map.xml -Force
```

The ```attribute-map.xml``` file specifies the *attributes* that are received from the IDP.

The keys used by the Service Provider when communicating with the IDP also need to be copied to the Shibboleth SP
directory on the Windows machine:

```powershell
>  Copy-Item /shib-service-provider/sp-demo/demo-sp-deployment-windows-iis-3_0/Shibboleth/metadatasigner.pem C:/opt/shibboleth-sp/etc/shibboleth/metadatasigner.pem -Force
>  Copy-Item /shib-service-provider/sp-demo/demo-sp-deployment-windows-iis-3_0/Shibboleth/sp-cert.pem C:/opt/shibboleth-sp/etc/shibboleth/sp-cert.pem -Force
>  Copy-Item /shib-service-provider/sp-demo/demo-sp-deployment-windows-iis-3_0/Shibboleth/sp-key.pem C:/opt/shibboleth-sp/etc/shibboleth/sp-key.pem -Force
```

* **sp-cert.pem** - the Service Provider *public* key
* **sp-key.pem** - the Service Provider *private* key

In case other keys are provided, it is important to copy the keys to the Shibboleth SP directory
(```C:/opt/shibboleth-sp/etc/shibboleth/```) - which is what happens in the ```Copy-Item``` command above.

The Shibboleth SP keys are configured in the ```C:\opt\shibboleth-sp\etc\shibboleth\shibboleth2.xml``` file:

```
<CredentialResolver type="File" key="sp-key.pem" certificate="sp-cert.pem"/>
```

Now, it is possible to login to newly configured Shibboleth SP.

## Login
Access the _secure_ resource (page) - https://demo.test1.samleiki.fo/Home/Secure - in the browser in order to login.
The user is asked to login at the IDP.

Once the user is logged in, it is possible to display all Server Variables by clicking on the **Show Server Variables...**
button.

## Remarks
It is important to note that the Shibboleth SP only ensures the authentication of users against an IDP in a secure
fashion ensured by the SAML 2.0 protocol. Once the user is logged in at the IDP level, the user must also be
authenticated on the Shibboleth SP web site. This authentication needs to be implemented on the web site (SP), and would
typically be utilizing the *Shibboleth* Server Variables, in order to make a decision on whether or not to grant the
user access to the protected resource. This topic is not covered in this guide.

In case the Service Provider is to require a login even though the user has already logged in previously at another 
Service Provider in the federation, it is possible to set a *forceAuthn* flag which will enforce such behaviour.

The *forceAuthn* flag can be set on the *RequestMapper* element and any of its child elements in the shibboleth2.xml
configuration file, e.g.:

```xml
<RequestMapper type="Native">
    <RequestMap forceAuthn="true">
        <Host name="demo.test1.samleiki.fo">
            <Path name="Home/Secure" authType="shibboleth" requireSession="true"/>
        </Host>
    </RequestMap>
</RequestMapper>
```

## Troubleshooting
Once the Shibboleth SP is installed, and up and running, some additional configuration of e.g. the firewall or IIS web 
server may still be needed in case *login* or *signout* scenarios are not *working properly*. 

In the SAML 2.0 protocol, the messages - e.g. the **Logout** request message issued by the IDP - may result in a 
*large* URL, since the actual SAML 2.0 request payload is inserted as a query string 
parameter into the URL. It is important that the URL is not altered, e.g. by firewall rules or web server settings or even
other *network mechanisms* in order for the SAML 2.0 protocol to function properly. 

Some things to be aware of: 

1. **Firewall:** It is important that the firewall or other *network mechanisms* do not alter or shorten the full URL
and Query String. 
2. **IIS URL and Query String size limits:** One thing to be aware of, when hosting on the IIS web server, are the
configuration settings that set the maximum limits on URL and query string sizes. If the maximum limit of the URL is 
reached in a request, the IIS will return a HTTP status code of 404 indicating that the resource could not be found.
The sample web application shipped with this demo overrides the default setting in its 
[Web.config](/demo-sp-deployment-windows-iis-3_0/Release/WebApplication1/Web.config) file by modifying the 
**<httpRuntime>** and **<requestLimits>** elements in the configuration file.

[comment]: () "The following are references to images used in this readme"
[asp-net-mvc-sp-screenshot]: Documentation/asp-net-mvc-sp-screenshot.png "Logo Title Text 2"
[shibboleth-site-handler-mapping]: Documentation/shibboleth-site-handler-mapping.png "Logo Title Text 2"