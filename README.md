# Talgildur Samleiki
## Examples of Shibboleth-based Service Providers

Version (Draft): 0.9 - 03.06.2019

---

This project describes the SAML 2.0 federation defined for Talgildur Samleiki, and provides three examples of service
providers - one for Apache in Docker, one for Windows IIS based on Shibboleth SP 2.0, and one for Windows IIS based 
on Shibboleth SP 3.0.

# Federation

This project includes a detailed description of how to setup and configure SAML 2.0 service providers based on the
Shibboleth Service Provider implementation. The service providers are pre-configured to be added to an existing SAML 2
federation (_used only for testing_) using an existing Identity Provider configured and hosted by Talgildur Samleiki.
The _metadata_ of the service provider and identity provider is key for adding new service providers to a SAML 2
federation - more on this subject in the following section.

It is possible to run the service provider on a local computer or server which eases the process of testing and
verifying the correctness of the service provider setup and configuration considerably.

Once the service provider has been correctly configured and setup following these guides, it is possible to login 
using either the IOS or Android app provided by Talgildur Samleiki.

## SAML 2 and Metadata

As mentioned earlier, the service provider and identity provider need to exchange `metadata` in order to build trust
in the federation. In short the metadata incorporates detailed information about an _entity_ in the federation, such as
relevant endpoints, signing and encryption keys. These information are in turn used when it comes to exchanging 
sensitive information over the web in a secure manner as required and specified by the SAML 2 protocol. 

The metadata for the identity provider in Talgildur Samleiki test environment is available in the link below:

* Talgildur Samleiki Test Identity Provider Metadata: https://innrita.test1.samleiki.fo/metadata.xml

The metadata of the identity provider describes the federation, and is digitally signed by a private key held by the 
identity provider itself. It is possible to verify the signature of the metadata by using the corresponding public key
which can be found in the link below:

* Public key of the Test Identity Provider:
[metadatasigner.pem](./demo-sp-deployment-apache/src/main/resources/config/sp/ssl/metadatasigner.pem)

The metadadata for the identity provider contains certain _attributes_ which describe each service provider in the
federation, e.g.:

* A unique identification of the service provider, i.e. the `Entity ID` 
* Display text which describes the name, purpose, and a link to a logo
* The public key which is required by the SAML 2 protocol for verifying the validity of messages sent back and forth 
in the federation between the parties
* URL endpoints, e.g. used for login and logout
 
As mentioned earlier, these guides contain metadata for a service provider that is already part of the Talgildur Samleiki
_test_ federation, and has the following entity ID:

* Sample Service Provider Entity ID: `https://demo.test1.samleiki.fo/shibboleth`  
 
In case a different Entity ID is needed, it is possible to make such a request by attaching the service provider 
`metadata` in an e-mail to [metadata@samleiki.fo](mailto:dev@samleiki.fo).

# Sample Service Providers

## Configuration and Limitations

Since these guides are limited only to describing the setup of a sample service provider, not all configuration features
and capabilities available in the Shibboleth SAML 2 service provider are described.

However, some of the key _features_ in the Shibboleth 2 Service Provider are:

* Only accepting signed _assertions_ from the identity provider
* Only accepting encrypted _assertions_
* Requiring digitally signed metadata only
* Description of required _attributes_
* Single Log-out
* Export of _header_ variables 

On the other hand, these guides do not explain capabilities such as:

* Exporting _environment variables_ using the [AJP protocol](https://wiki.shibboleth.net/confluence/display/SP3/JavaHowTo)
* [Assertion export](https://wiki.shibboleth.net/confluence/display/SP3/AssertionExport)

The complete documentation for configuring a Shibboleth Service Provider is located here on the Shibboleth website:

* Shibboleth Wiki: https://wiki.shibboleth.net/confluence/display/SP3/

## Apache in Docker

For a detailed description on how to run a Shibboleth Service Provider using Apache, follow the link below:
 
* [Shibboleth Service Provider in Apache](./demo-sp-deployment-apache/README.md)

## Windows IIS - Shibboleth SP 2.0

For a detailed description on how to run a Shibboleth Service Provider 2.0 in Windows using Internet Information Services
(IIS), follow the link below:
 
* [Shibboleth Service Provider 2.0 in Windows (IIS)](./demo-sp-deployment-windows-iis/README.md)

## Windows IIS - Shibboleth SP 3.0 (Recommended for all new Service Providers)

For a detailed description on how to run a Shibboleth Service Provider 3.0 in Windows using Internet Information Services
(IIS), follow the link below:
 
* [Shibboleth Service Provider 3.0 in Windows (IIS)](./demo-sp-deployment-windows-iis-3_0/README.md)

## Extract public- and private key from P12 file
To extract the keypair from the P12 keystore, the following commands should be executed.

##### Private key

    openssl pkcs12 -in file.p12 -nocerts -nodes -out privateKey.pem

##### Public key

    openssl pkcs12 -in file.p12 -clcerts -nokeys -out publicCert.pem

