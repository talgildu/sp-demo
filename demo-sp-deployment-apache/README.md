# Apache Sample Service Provider

Version (Draft): 0.9 - 06.06.2019

---

## Introduction

This guide describes the process of running a Shibboleth Service Provider in Apache using a Linux distribution hosted
in a Docker container.

## Requirements

It is required that Docker CE and docker-compose is installed on the machine before running through this guide. More
information about these technologies can be found on the Docker web sites - www.docker.com
(e.g. https://docs.docker.com/ and https://docs.docker.com/compose/install/)

The following lists the technology stack used in the Docker file being applied in this guide:

* CentOS 7.4 Linux distribution 
* Apache HTTP 2.4, https://httpd.apache.org/
* PHP 5.4
* Openssl 1.0.2k
* Shibboleth Service Provider 3, http://www.shibboleth.net

The versions of Apache HTTP, PHP, and OpenSSL that are being installed in the Docker file are identical to the ones
that ship with the CentOS 7.4 Docker image. It is required that the logged in user on the machine has write access to the
`hosts` file in order that a new mapping can be made that points the service provider domain to localhost (127.0.0.1).

## The Assets

The files in this project are organized in two main areas/directories:

* __/dist__ - contains the pre-configured configurations of the Shibboleth Service Provider
* __/src__ - contains the basic configurations used for making custom adjustments

When downloading (cloning) this project from Bitbucket it is possible to test-run the service provider directly from
the __/dist__ directory by going through the following steps (_each step is explained in more detail in the upcoming 
sections_):

1. Build the Docker-image that forms the base for the service provider installation
2. Adjust the `hosts` file on the local machine such that `demo.test1.samleiki.fo` points to 127.0.0.1 (localhost)
3. Startup the service provider using docker-compose

Once the steps above have been performed, it is possible to go to the service provider URL (in a browser) on 
http://demo.test1.samleiki.fo.

The following sections will go through each of the steps above in more detail.

#### Docker File

The files needed to build the Docker file are located in the `/dist/docker` directory. Navigate to this directory and 
run the following commands in order to build the Docker file:

    docker build -t demo-sp-dev .

#### Adjusting the `hosts` File

In order to make the service provider accessible on the local machine on the specified URL, it is necessary to make a
`hosts` mapping that points the name, `demo.test1.samleiki.fo`, to 127.0.0.1 (localhost).  

#### Starting the Service Provider

The Service Provider can be started by navigating to the `/dist` directory and executing the following command:

    docker-compose up

## Custom Configuration

This project is organized in such a manner, that it is possible to make adjustments to the configuration of the 
Service Provider.

In order to do so some knowledge of [Apache Maven](https://maven.apache.org/) is required.

The configuration is located in the `/src/main/resources` directory, and is organized into 6 parts: 
   
|  Directory |  Description |
|---|---|
| /config/sp/shibboleth  | Shibboleth Service Provider configuration  |
| /config/sp/ssl | Keys and certificate |
| /config/sp/httpd  | Apache HTTPD web server configuration |
| /config/www | Web page files |
| /docker/ | Files relevant to the docker-image  |
| docker-compose.yml  | Docker Compose configuration file |

Instead of manipulating the Shibboleth configuration files directly, the settings have been made available through
template variables. This makes it possible to control the configuration by setting the relevant variables in the 
`pom.xml` file.
 
This approach yields a better overview of all the possible settings available in this project. Applying the settings
in the `pom.xml` file is simply a question of running the following command: 

     mvn compile
     
The result of running this command is a service provider - located in the `/dist` directory - whose configuration
is reflected by the variables that have been used in the `pom.xml` file.

The list below shows all the template variables that are configurable through the `pom.xml` file:

        <maven.sg.entityID>https://demo.test1.samleiki.fo/shibboleth</maven.sg.entityID>
        <maven.sg.discoveryURL>https://demo.test1.samleiki.fo/innrita/</maven.sg.discoveryURL>
        <maven.sg.ServerName>demo.test1.samleiki.fo</maven.sg.ServerName>
        <maven.sg.ServerAlias>demo</maven.sg.ServerAlias>
        <maven.sg.SSLCertificateFile>/etc/pki/tls/certs/server.crt</maven.sg.SSLCertificateFile>
        <maven.sg.metadatasigner>/etc/pki/tls/certs/metadatasigner.pem</maven.sg.metadatasigner>
        <maven.sg.metadata.reloadinterval>3600</maven.sg.metadata.reloadinterval>
        <maven.sg.session.timeout>3600</maven.sg.session.timeout>
        <maven.sg.session.lifetime>28800</maven.sg.session.lifetime>
        <maven.sg.clockSkew>1800</maven.sg.clockSkew>
        <maven.sg.metadata.url>https://innrita.test1.samleiki.fo/metadata.xml</maven.sg.metadata.url>